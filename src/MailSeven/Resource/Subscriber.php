<?php
namespace MailSeven\Resource;

use MailSeven\Resource;

class Subscriber extends Resource {

    /**
     * @var string
     */
    protected $resourceName = 'subscriber';

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @param string $email
     */
    public function __construct($email = '') {
        if ($email !== '') {
            $this->email = $email;
        }
    }
}