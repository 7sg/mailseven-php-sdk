<?php
namespace MailSeven;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;

class Api {

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $endPoint;

    /**
     * @var string
     */
    protected $format = 'json';

    public function __construct($key, $secret, $endPoint) {
        $this->key = $key;
        $this->secret = $secret;
        $this->endPoint = \rtrim($endPoint,'/');
    }

    /**
     * @param ResourceInterface $resource
     * @throws \Exception
     */
    public function create($resource) {
        $uri = new Uri($this->getResourceUri('create', $resource->getResourceName()));
        $this->post($uri, $resource->toArray());
    }

    /**
     * @param string $resourceName
     * @param array $parameters
     */
    public function subscribeTo($resourceName, $parameters) {
        $uri = new Uri($this->getResourceUri('subscribeTo', $resourceName));
        $this->post($uri, $parameters);
    }

    /**
     * @param string $resourceName
     * @param array $parameters
     */
    public function unsubscribeFrom($resourceName, $parameters) {
        $uri = new Uri($this->getResourceUri('unsubscribeFrom', $resourceName));
        $this->post($uri, $parameters);
    }

    /**
     * @param Uri $uri
     * @param array $parameters
     * @throws \Exception
     */
    protected function post($uri, $parameters) {
        try {
            $client = new Client();

            $response = $client->post($uri, [
                'form_params' => $parameters,
                'auth' => [$this->key, $this->secret]
            ]);

            if ($response->getStatusCode() !== 200) {
                throw new \Exception($response->getStatusCode());
            }

            $body = \json_decode($response->getBody(), true);

            if (!isset($body['success'])) {
                throw new \Exception($response->getBody()->__toString());
            }
        } catch(\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $resourceName
     * @return mixed
     */
    public function get($resourceName) {
        $uri = new Uri($this->getResourceUri('get', $resourceName));

        $client = new Client();

        $response = $client->get($uri, [
            'auth' => [$this->key, $this->secret]
        ]);

        $body = \json_decode($response->getBody(), true);

        return $body;
    }

    /**
     * @param string $action
     * @param string $resourceName
     * @return string
     */
    protected function getResourceUri($action, $resourceName) {
        $resourceUri = $this->endPoint . '/api/' . \strtolower($action . $resourceName) . '.' . $this->format;

        return $resourceUri;
    }
}