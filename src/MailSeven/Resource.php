<?php
namespace MailSeven;

class Resource implements ResourceInterface {

    /**
     * @var string
     */
    protected $resourceName = '';

    /**
     * @return string
     */
    public function getResourceName() {
        return $this->resourceName;
    }

    /**
     * @param string $resourceName
     */
    public function setResourceName($resourceName) {
        $this->resourceName = $resourceName;
    }

    /**
     * @return array
     */
    public function toArray() {
        $class = new \ReflectionClass($this);
        $properties = [];

        foreach ($class->getProperties() as $property) {
            if ($property->isPublic()) {
                if (empty($property->getValue($this)) === false) {
                    $properties[$property->getName()] = $property->getValue($this);
                }
            }
        }

        if ($this->resourceName !== '') {
            return [\strtolower($class->getShortName()) => $properties];
        } else {
            return $properties;
        }
    }
}