<?php
namespace MailSeven;

interface ResourceInterface {

    public function getResourceName();

    public function toArray();
}